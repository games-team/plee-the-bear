plee-the-bear (0.7.1-1) UNRELEASED; urgency=medium

  * Team upload.
  * Remove retired uploader Gonéri Le Bouder, thank you for your work
  * Add d/gbp.conf
  * Switch to alive fork, already packaged by Fedora, SuSE etc...
    (Closes: #1053097)
  * New upstream version 0.7.1
  * Switch to SDL2 (Closes: #1038539)
  * Redo d/rules with modern DebHelper dispatcher

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable):
    + Build-Depends: Drop versioned constraint on cmake, gettext,
      libboost-filesystem-dev, libboost-regex-dev, libboost-thread-dev,
      libclaw-application-dev, libclaw-configuration-file-dev, libclaw-dev,
      libclaw-dynamic-library-dev, libclaw-graphic-dev, libclaw-logger-dev,
      libclaw-net-dev, libclaw-tween-dev, libsdl-mixer1.2-dev and
      mesa-common-dev.

 -- Alexandre Detiste <tchet@debian.org>  Fri, 08 Nov 2024 17:01:45 +0100

plee-the-bear (0.6.0-8) unstable; urgency=medium

  * Team upload.
  * Transition to wxWidgets 3.2 (Closes: #1019804)

 -- Scott Talbert <swt@techie.net>  Thu, 27 Oct 2022 20:07:32 -0400

plee-the-bear (0.6.0-7) unstable; urgency=medium

  * Team upload.

  [ Reiner Herrmann ]
  * Mark -data package as Multi-Arch: foreign

  [ Markus Koschany ]
  * Switch to debhelper-compat = 13.
  * Declare compliance with Debian Policy 4.5.0.
  * Fix FTBFS with Boost 1.71.
    Thanks to Giovanni Mascellani for the patch. (Closes: #953871)
  * Fix FTCBFS: Let dh_auto_configure pass cross flags to cmake.
    Thanks to Helmut Grohne for the patch. (Closes: #918038)

 -- Markus Koschany <apo@debian.org>  Fri, 05 Jun 2020 01:39:41 +0200

plee-the-bear (0.6.0-6) unstable; urgency=medium

  * Team upload.
  * Call executables by absolute path in .desktop files instead of making the
    desktop environment consolt the PATH environment variable. Added
    absolute-path-in-desktop-file.patch.

 -- Bruno Kleinert <fuddl@debian.org>  Sat, 10 Aug 2019 05:48:24 +0200

plee-the-bear (0.6.0-5) unstable; urgency=medium

  * Team upload.
  * Build package against GTK 3 variant of wxWidget. Replaced build dependency
    libwxgtk3.0-dev by libwxgtk3.0-gtk3-dev. (Closes: #933443)
  * Clean up debian/control:
    * Change priority from extra to optional.
    * Reflect migration to salsa.debian.org in Vcs-fields.
  * Clean up lintian warnings: Removed unused patches.
  * Switch from debian/compat to build depends debhelper-compat (= 12).
  * Declare conformance with standards version 4.4.0.

 -- Bruno Kleinert <fuddl@debian.org>  Fri, 09 Aug 2019 16:07:02 +0200

plee-the-bear (0.6.0-4) unstable; urgency=medium

  * Team upload.
  * Switch to compat level 10.
  * Declare compliance with Debian Policy 4.0.1.
  * Vcs-Browser: Use https.
  * Rebuild plee-the-bear with GCC 7. (Closes: #853618, #793274)
  * Replace dh_clean -k with dh_prep.

 -- Markus Koschany <apo@debian.org>  Fri, 18 Aug 2017 18:27:03 +0200

plee-the-bear (0.6.0-3.1) unstable; urgency=low

  * Non-maintainer upload at maitainer's request.
  * Update to use wxWidgets 3.0 (new patch wx3.0-compat.patch)
    (Closes: #750087)

 -- Olly Betts <olly@survex.com>  Fri, 25 Jul 2014 10:58:41 +1200

plee-the-bear (0.6.0-3) unstable; urgency=medium

  * Build on all archs; do not restrict Architecture in d/control.
    (Closes: #736537)

 -- Vincent Cheng <vcheng@debian.org>  Fri, 24 Jan 2014 12:57:10 -0800

plee-the-bear (0.6.0-2) unstable; urgency=medium

  * Team upload.

  [ Evgeni Golov ]
  * Correct Vcs-* URLs to point to anonscm.debian.org

  [ Gonéri Le Bouder ]
  * import patches by Julien Jorge to fix a FTBFS  with the current
    Boost.FileSystem (closes: #720819)
  * Add myself in Uploaders
  * Indent the B-D

  [ Julien Jorge ]
  * Add mipsn32el mips64 mips64el in the architecures (closes: #726176)
  * Add a patch to use the full path to the icon in the menu files
    (closes: #726853)

  [ Vincent Cheng ]
  * Refresh patches.
  * Update to Standards version 3.9.5.
  * Update to source format "3.0 (quilt)".

 -- Vincent Cheng <vcheng@debian.org>  Thu, 23 Jan 2014 13:20:52 -0800

plee-the-bear (0.6.0-1) unstable; urgency=low

  * New upstream release.
  * Remove patches integrated upstream: boost-1.46.diff,
    compile-with-g++4.6.diff, use-findpackage-for-libclaw.diff.

 -- Julien Jorge <julien.jorge@gamned.org>  Sat, 27 Aug 2011 19:25:18 +0200

plee-the-bear (0.5.1-2) unstable; urgency=low

  * Use the findpackage command to find libclaw, according to the new
    installation path of the CMake module of the library.
  * Compiles with g++ 4.6.
  * Fix FTBFS with Boost 1.46. Thanks to Konstantinos Margaritis.
    (Closes: #627253).
  * Bump standard version to 3.9.2.

 -- Julien Jorge <julien.jorge@gamned.org>  Sun, 21 Aug 2011 18:07:35 +0200

plee-the-bear (0.5.1-1) unstable; urgency=low

  [ Julien Jorge ]
  * New upstream release (Closes: #565062, #546514).
  * Add armhf architecture (Closes: #604689).
  * Remove patches integrated upstream: rpath-editors.diff, rpath-game.diff,
    editors-menu-section.diff.
  * Bump the Standard-Version, no changes.
  * Update my email address.
  * Set build dependency of libclaw to 1.6.0.
  * Add the missing ${misc:Depends} in debian/control.
  * List gettext translations in bear-factory.install and plee-the-bear.install.

 -- Julien Jorge <julien.jorge@gamned.org>  Wed, 17 Nov 2010 20:13:34 +0200

plee-the-bear (0.4.1-3) unstable; urgency=low

  [ Julien Jorge ]
  * Remove the 'Section' line in the control file for bear-factory.
    Thanks Ben Finney (Closes: #550224).
  * Install the binaries of the editors in /usr/games/
  * Add a patch to change section in menu files of the editors from
    Apps/Editors to Games/Tools.
  * Bug fix in patch editors-default-dir.diff to effectively set the
    default paths.

 -- Julien Jorge <julien_jorge@yahoo.fr>  Thu, 08 Oct 2009 16:27:44 +0200

plee-the-bear (0.4.1-2) unstable; urgency=low

  [ Julien Jorge ]
  * Add wx2.6-headers in Build-Conflicts
  * Remove \n in the Architecture line.
    Thanks Cyril Brulebois (Closes: #545913).

 -- Julien Jorge <julien_jorge@yahoo.fr>  Thu, 10 Sep 2009 11:12:28 +0200

plee-the-bear (0.4.1-1) unstable; urgency=low

  [ Julien Jorge ]
  * New upstream release (Closes: #543328, #545603)
  * Remove the patch clear-selection-checks-layer.diff, fixed upstream.
  * Remove the Creative Commons Sampling Plus licence text. Ressources
    have been replaced by free sounds.

 -- Julien Jorge <julien_jorge@yahoo.fr>  Sat, 03 Sep 2009 00:41:24 +0200

plee-the-bear (0.4-1) unstable; urgency=low

  [ Julien Jorge ]
  * New upstream release.
  * Thanks Cyril Brulebois
  * Preparing the package for the release.
  * Add a new package for the editors.
  * Update installation files.
  * Remove patches fix-create-file-in-current-dir.diff, numeric_limits.diff,
    use_boost_filesystem_mt.diff, fixed in upstream source.
  * Remove dont-compile-extra-stuff.diff so the editors are available.
  * Remove manpages creation from rules, now created by upstream.
  * Use correct CMake variables to define installation paths.
  * Set copyright to 2009.
  * Fix watch file regexp.
    + Probably still needs improvement.
  * Fix a segfault when clicking on an empty level in the level editor
    (patch clear-selection-checks-layer.diff).
  * Add a -DNDEBUG on the compile line.
  * Add libboost-thread in the dependencies (required by version 0.4).
  * Install private libraries of the editor in /usr/lib/bear-factory.
  * Set the correct rpath for the editors (patch rpath-editors.diff).
  * Set the correct rpath for the game (patch rpath-game.diff).
  * Set the default paths of the datas in the editors
    (patch editors-default-dir.diff).
  * Add the Creative Commons Sampling Plus license in the copyright file
    (used by samples comming from The Freesound Project).
  * Do not install AUTHORS files (patch do-not-install-authors-files.diff).

 -- Julien Jorge <julien_jorge@yahoo.fr>  Sat, 22 Aug 2009 10:59:56 +0200

plee-the-bear (0.2.1-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix FTBFS with new boost (>= 1.37.0) by using the “-mt” suffix for
    boost libraries since starting with this version, only this variant
    is available (Closes: #530477, #534039):
     - Add patch: fix-ftbfs-with-new-boost.diff
  * Not bumping urgency, the package isn't in testing yet.
  * Get rid of -pN options in series file, -p1 is the default, and that
    could lead to some troubles with the 3.0 format.

 -- Cyril Brulebois <kibi@debian.org>  Sat, 18 Jul 2009 13:46:25 +0200

plee-the-bear (0.3.1-1) UNRELEASED; urgency=low

  [ Julien Jorge ]
  * New upstream release

  [ Eddy Petrișor ]
  * Put the Debian Games Team in the Maintainer field

  [ Ansgar Burchardt ]
  * debian/control: Remove redundant Homepage fields
  * debian/control: Change short description slightly

  [ Barry deFreese ]
  * Add myself to uploaders.
  * use_boost_filesystem_mt.diff. (Closes: #534039, #530477).
    + Use multithreaded boost lib.
  * numeric_limits.diff - Fix FTBFS with missing includes.
  * Minor syntax fix in debian/copyright.
  * Remove first person from package description.
  * Remove duplicate Section fields from binary packages.
  * Fix watch file regexp.
    + Probably still needs improvement.
  * Change package description for -data package.
  * Bump Standards Version to 3.8.2. (No changes needed).

 -- Barry deFreese <bdefreese@debian.org>  Fri, 10 Jul 2009 09:19:23 -0400

plee-the-bear (0.2.1-2) unstable; urgency=high

  [ Julien Jorge ]
  * Do not create files in current directory (Closes: #490757)
  * Deactivate architectures alpha, arm, armel, powerpc and s390, not
    supported yet by upstream (Closes: #490020)

  [ Gonéri Le Bouder ]
  * For compatibility with the source "3.0 (quilt)" source package format,
    fix fix_.desktop.diff and plee-the-bear.desktop so now they can be applied
    with -p1.

 -- Julien Jorge <julien_jorge@yahoo.fr>  Sat, 26 Jul 2008 00:45:13 +0200

plee-the-bear (0.2.1-1) unstable; urgency=low

  * Initial release (Closes: #482663)

 -- Julien Jorge <julien_jorge@yahoo.fr>  Sun, 25 May 2008 11:08:32 +0200

